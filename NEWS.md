# tRigon 0.3.3

* Updated author information and publication in contact data.

# tRigon 0.3.2

* Initial CRAN submission.
